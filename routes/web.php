<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('candidates','CandidatesController')->middleware('auth');
Route::get('candidate/changeuser/{cid}/{uid?}','CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatus/{cid}/{sid}', 'CandidatesController@changeStatus')->name('candidate.changestatus')->middleware('auth');

Route::get('candidate/delete/{id}','CandidatesController@destroy')->name('candidate.delete');

Route::get('/profile', 'CandidatesController@getCandidate')->name('candidate.getcandidate')->middleware('auth');
Route::get('/hello',function(){
    return 'Hello Laravel';
});

Route::get('/student/{id}', function($id = 'No student found'){
    return 'we got student with id'.$id ;
});

Route::get('/car/{id?}', function($id = null){
    if(isset($id)){
        //TODO: validate for interger
        return "we got car $id";
    }else {
        return " we need the id to find your id";
    }
});

Route::get('/comment/{id}',function($id){
    return view('comment',compact('id'));
});

Route::get('/users/{email}/{name?}',function($email,$name='null'){
    if(!isset($name)){ 
        $name='name missing';   
    }
    return view('users', compact('email','name'));
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');