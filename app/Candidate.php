<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    //איזה שדות אני מרשה לעדכן
    protected $fillable = ['name','email'];
    #יוצר את החיבור בין 
    #candidate to User
    public function owner(){
        return $this->belongsTo('App\User','user_id'); // each candidate has only one user (owner)
        }
        
    public function situation(){ 
        return $this->belongsTo('App\Status', 'status_id'); // each candidate has only one status
        }
}