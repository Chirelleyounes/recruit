@extends('layouts.app')
@section('title','Edit Candidates')
@section('content')
    <body>
    <h1>Edit Candidate <br> ID: {{$id}}</h1>
        @if (count($errors) > 0)
        <div class="alert alert -danger">
            <ul>
                @foreach ($errors -> all() as $error)
                    <li>{{$error}}</li>    
                @endforeach
            </ul>
        @endif
        <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @csrf
        <input type="hidden" name="_method" value="PATCH" />
     <!-- איזה ערכים הולכים לשנות בEDIT -->
        <div>
            <label for = "name">Candidate name</label>
            <input type = "text" name = "name" value = {{$candidate->name}}>
        </div>
        <div>
            <label for = "email">Candidate email</label>
            <input type = "text" name = "email" value = {{$candidate->email}}>
        </div>
        <div>
            <input type = "submit" name = "submit" value = "Update candidate">
        </div>
        </form>      
    </body>
@endsection