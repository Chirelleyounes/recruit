<?php

use Illuminate\Database\Seeder;
class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'name' => 'before interview',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'not fit',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'sent to manager',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'not fit professionally',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'name' => 'accepted to work',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);
    }
}